# pwt-0x01-ng

this repo holds *sawce* for PWT .netcore mvc project 0x01-ng

### how to run this
- Makefile (you need `make` for this) --> see the [[Makefile]]
- direct `dotnet` (and/or `docker`) commands

#### useful Makefile targets
- `restore` --> runs `dotnet restore .`
- `clean` --> clean builds the project
- `build` --> builds the project
- `dockerdevbuild` --> clean-builds a container image from [[Dockerfile.dev]] (have a look in there for details)
- `dcdevb` --> compose clean build of a debug (dev) version, pulling db
- `dcdevup` --> runs the above (see [[docker-compose.yml]])
> you need to have created a db beforehand to run these
- `run` --> runs `$CURRENT_ENV dotnet watch run .`
- `dev` --> runs `restore build run` - this is kind of a convenience target

#### run using dotnet
> run the `dotnet` commands from the solution folder
> note that a preconfigured db is required for any kind of running the project (consider taking a look at [useful makefile targets](#useful-makefile-targets))

on the first run, restore stuff
```sh
dotnet restore
```

build and run
```sh
dotnet build && dotnet run
```
#### errors
if you get a weird long error about not being able to listen bind a port, make sure nothing else is listening on the port *this* thing is trying to bind (tcp/5000).  
if something else *is* already listening, solve it by killing it before running `dotnet run` or change the app port in [[Properties/launchSettings.json]]
