using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using pwt_0x01_ng.Models;
using pwt_0x01_ng.Models.Database;

namespace pwt_0x01_ng.Controllers
{
	public class ProductsController : Controller
	{
		readonly DBContext dbctx;

		public ProductsController(DBContext dbctx){
			this.dbctx = dbctx;
		}
		public async Task<IActionResult> Detail(int id){
			Product p = await dbctx.Product.Where(p_item => p_item.id == id).FirstOrDefaultAsync();
			if (p == null) return NotFound();

			IList<Similar> s = await dbctx.Similar.Where(p_item => p_item.prod_id == id).ToListAsync();
			IList<Product> similar = new List<Product>();
			if (s != null) {
				foreach (var s_item in s) {
					var prod = await dbctx.Product.Where(p_item => p_item.id == s_item.similar_prod_id).FirstOrDefaultAsync();
					if (prod != null) {
						similar.Add(prod);
					}
				}
			}

			if (similar.Count > 0) {
				ViewData["similar"] = similar;
			}
			return View(p);
		}
	}
}
