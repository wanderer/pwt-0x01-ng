﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using pwt_0x01_ng.Models;
using pwt_0x01_ng.Models.Database;

namespace pwt_0x01_ng.Controllers
{
	public class HomeController : Controller
	{
		readonly DBContext dbctx;
		readonly ILogger<HomeController> logger;
		public HomeController(DBContext dbctx, ILogger<HomeController> logger){
			this.dbctx = dbctx;
			this.logger = logger;
		}

		public async Task <IActionResult> Index()
		{
			UltimateViewModel uvm = new UltimateViewModel();
			uvm.Carousels = await dbctx.Carousel.ToListAsync();
			uvm.Products = await dbctx.Product.ToListAsync();
			return View(uvm);
		}

		public IActionResult About()
		{
			ViewData["Message"] = "Best project ever";

			return View();
		}

		public IActionResult Contact()
		{
			ViewData["Message"] = "Where you can find us";
			logger.LogInformation("yay - somebody looking for our contact info");

			return View();
		}

		public IActionResult Privacy()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
		}

		[Route("/Home/HandleError/{code:int}")]
		public IActionResult HandleError(int code)
		{
			ViewData["ErrorMessage"] = $"Error {code}";
			switch(code) {
				case 403:
					return View("~/Views/Shared/403.cshtml");

				case 404:
					return View("~/Views/Shared/404.cshtml");

				case 500:
					return View("~/Views/Shared/500.cshtml");

				default:
					return View("~/Views/Shared/unhandled_err_code.cshtml");
			}
		}
	}
}
