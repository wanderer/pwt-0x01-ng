DROP TRIGGER IF EXISTS trigger_updated ON "Order";
CREATE OR REPLACE FUNCTION updatedattr() RETURNS trigger
	AS $func$
	BEGIN
	NEW."Updated" := now();
	NEW."Created" := OLD."Created";
	RAISE NOTICE 'Order updated ''%'' on %', OLD."Order_Number", NEW."Updated";
	RETURN NEW;
	END;
	$func$ LANGUAGE plpgsql;
	CREATE TRIGGER trigger_updated
	BEFORE UPDATE ON "Order"
	FOR EACH ROW
	EXECUTE PROCEDURE updatedattr();

DROP TRIGGER IF EXISTS trigger_updated ON "Product";
CREATE OR REPLACE FUNCTION updatedattr() RETURNS trigger
	AS $func$
	BEGIN
	NEW."Updated" := now();
	NEW."Created" := OLD."Created";
	RAISE NOTICE 'Product updated ''%'' on %', OLD."id", NEW."Updated";
	RETURN NEW;
	END;
	$func$ LANGUAGE plpgsql;
	CREATE TRIGGER trigger_updated
	BEFORE UPDATE ON "Product"
	FOR EACH ROW
	EXECUTE PROCEDURE updatedattr();

DROP TRIGGER IF EXISTS trigger_updated ON "OrderItem";
CREATE OR REPLACE FUNCTION updatedattr() RETURNS trigger
	AS $func$
	BEGIN
	NEW."Updated" := now();
	NEW."Created" := OLD."Created";
	RAISE NOTICE 'OrderItem updated ''%'' on %', OLD."id", NEW."Updated";
	RETURN NEW;
	END;
	$func$ LANGUAGE plpgsql;
	CREATE TRIGGER trigger_updated
	BEFORE UPDATE ON "OrderItem"
	FOR EACH ROW
	EXECUTE PROCEDURE updatedattr();

DROP TRIGGER IF EXISTS trigger_updated ON "Carousel";
CREATE OR REPLACE FUNCTION updatedattr() RETURNS trigger
	AS $func$
	BEGIN
	NEW."Updated" := now();
	NEW."Created" := OLD."Created";
	RAISE NOTICE 'Carousel updated ''%'' on %', OLD."id", NEW."Updated";
	RETURN NEW;
	END;
	$func$ LANGUAGE plpgsql;
	CREATE TRIGGER trigger_updated
	BEFORE UPDATE ON "Carousel"
	FOR EACH ROW
	EXECUTE PROCEDURE updatedattr();
