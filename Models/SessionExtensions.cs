using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

public static class SessionExtensions
{
	public static T GetObject<T>(this ISession session, string key)
	{
		var data = session.GetString(key);
		if (data == null)
		{
			return default(T);
		}
		return JsonConvert.DeserializeObject<T>(data);
	}

	public static void SetObject(this ISession session, string key, object value)
	{
		session.SetString(key, JsonConvert.SerializeObject(value));
	}
}
