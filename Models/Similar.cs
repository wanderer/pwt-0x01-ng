using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace pwt_0x01_ng.Models
{
	[Table(nameof(Similar))]
	public class Similar
	{
		[Required]
		[Key]
		public int id { get; set; }
		[Required]
		public int prod_id { get; set; }
		[ForeignKey(nameof(Product))]
		[Required]
		public int similar_prod_id { get; set; }
	}
}
