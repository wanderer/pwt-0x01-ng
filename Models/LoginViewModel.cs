using System.ComponentModel.DataAnnotations;
using pwt_0x01_ng.Models.Validation;

namespace pwt_0x01_ng.Models
{
	public class LoginViewModel
	{
		[Required]
		public string username {get; set;}
		[Required]
		[UniqueCharsAttr]
		public string password {get; set;}
		public bool login_failed {get; set;}		
	}
}