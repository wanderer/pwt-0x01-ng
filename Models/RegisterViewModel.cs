using System.ComponentModel.DataAnnotations;
using pwt_0x01_ng.Models.Validation;

namespace pwt_0x01_ng.Models
{
	public class RegisterViewModel
	{
		[Required]
		public string username {get; set;}
		[Required]
		[UniqueCharsAttr]
		[RegularExpression(@"^.{2,}$", ErrorMessage = "Minimum password length is 2 characters.")]
		public string password {get; set;}
		[Required]
		[Compare(nameof(password), ErrorMessage = "Passwords don't match.")]
		public string repeated_password {get; set;}
		[Required]
		public string email {get; set;}
	}
}