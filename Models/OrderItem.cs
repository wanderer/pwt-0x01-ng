using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace pwt_0x01_ng.Models
{
	[Table(nameof(OrderItem))]
	public class OrderItem : Entity
	{
		[ForeignKey(nameof(Order))]
		[Required]
		public int Order_id { get; set; }
		[ForeignKey(nameof(Product))]
		[Required]
		public int Product_id { get; set; }
		[Required]
		public int Amount { get; set; }
		[Required]
		public decimal Price { get; set; }
		public Order Order { get; set; }
		public Product Product {get; set; }
	}
}