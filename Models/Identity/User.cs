using Microsoft.AspNetCore.Identity;

namespace pwt_0x01_ng.Models.Identity
{
	public class User : IdentityUser<int>
	{
		public virtual string username {get;set;}
		public virtual string password {get;set;}
		public virtual string Name {get; set;}
		public virtual string LastName {get; set;}
	}
}