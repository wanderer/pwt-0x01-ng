using Microsoft.AspNetCore.Identity;

namespace pwt_0x01_ng.Models.Identity
{
	public class Role : IdentityRole<int>
	{
		public Role(string name) : base(name){}
	}
}