using System.Collections.Generic;

namespace pwt_0x01_ng.Models
{
	public class ProductViewModel
	{
		public IList<Product> Products { get; set; }
	}
}
