using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using pwt_0x01_ng.Models.Validation;

namespace pwt_0x01_ng.Models
{
	[Table("Product")]
	public class Product : Entity
	{
		[Required]
		public string Name { get; set; }
		[Required]
		public int Price { get; set; }
		[Required]
		public string Description { get; set; }
		[FileTypeAttr("image")]
		[NotMapped]
		public IFormFile Image { get; set; }
		[StringLength(255)]
		public string ImageSrc { get; set; }
		[Required]
		[StringLength(50, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 1)]
		public string ImageAlt { get; set; }
		[NotMapped]
		public IList<SimilarProduct> Similar { get; set; }
		[NotMapped]
		public bool Selected { get; set; }
	}
}
