using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace pwt_0x01_ng.Models.Database.Conf
{
	public class CarouselConf : IEntityTypeConfiguration<Carousel>
	{
		public void Configure(EntityTypeBuilder<Carousel> builder)
		{
			builder.Property(nameof(Carousel.Created)).ValueGeneratedOnAdd().HasDefaultValueSql("NOW()");
			builder.Property(nameof(Carousel.Updated)).ValueGeneratedOnAddOrUpdate().HasDefaultValueSql("NOW()");
		}
	}
}