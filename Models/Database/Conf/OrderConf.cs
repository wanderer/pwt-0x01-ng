using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace pwt_0x01_ng.Models.Database.Conf
{
	public class OrderConf : IEntityTypeConfiguration<Order>
	{
		public void Configure(EntityTypeBuilder<Order> builder)
		{
			builder.HasMany(order => order.OrderItems).WithOne(item => item.Order).IsRequired().HasForeignKey(item => item.Order_id).OnDelete(DeleteBehavior.Restrict);
			builder.Property(nameof(Order.Created)).ValueGeneratedOnAdd().HasDefaultValueSql("NOW()");
			builder.Property(nameof(Order.Updated)).ValueGeneratedOnAddOrUpdate().HasDefaultValueSql("NOW()");
		}
	}
}