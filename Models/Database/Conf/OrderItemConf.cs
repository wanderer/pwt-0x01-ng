using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace pwt_0x01_ng.Models.Database.Conf
{
	public class OrderItemConf : IEntityTypeConfiguration<OrderItem>
	{
		public void Configure(EntityTypeBuilder<OrderItem> builder)
		{
			builder.Property(nameof(OrderItem.Created)).ValueGeneratedOnAdd().HasDefaultValueSql("NOW()");
			builder.Property(nameof(OrderItem.Updated)).ValueGeneratedOnAddOrUpdate().HasDefaultValueSql("NOW()");
		}
	}
}