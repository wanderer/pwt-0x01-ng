using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace pwt_0x01_ng.Models.Database.Conf
{
	public class ProductConf : IEntityTypeConfiguration<Product>
	{
		public void Configure(EntityTypeBuilder<Product> builder)
		{
			builder.Property(nameof(Product.Created)).ValueGeneratedOnAdd().HasDefaultValueSql("NOW()");
			builder.Property(nameof(Product.Updated)).ValueGeneratedOnAddOrUpdate().HasDefaultValueSql("NOW()");
		}
	}
}