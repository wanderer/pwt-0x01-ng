﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using pwt_0x01_ng.Models.Identity;
using pwt_0x01_ng.Models.Database.Conf;

namespace pwt_0x01_ng.Models.Database
{
	public class DBContext : IdentityDbContext<User, Role, int>
	{
		public DBContext(DbContextOptions<DBContext> options) : base(options)
		{

		}

		public DbSet<Carousel> Carousel { get; set; }
		public DbSet<Product> Product { get; set; }
		public DbSet<Order> Order { get; set; }
		public DbSet<OrderItem> OrderItem { get; set; }
		public DbSet<Similar> Similar { get; set; }

		protected override void OnModelCreating(ModelBuilder model_builder)
		{

			base.OnModelCreating(model_builder);
			this.ApplyConfiguration(model_builder);

			foreach(var entity_type in model_builder.Model.GetEntityTypes()){
				entity_type.SetTableName(entity_type.GetTableName().Replace("AspNet", string.Empty));
			}
		}

		protected virtual void ApplyConfiguration(ModelBuilder model_builder)
		{
			model_builder.ApplyConfiguration(new CarouselConf());
			model_builder.ApplyConfiguration(new ProductConf());
			model_builder.ApplyConfiguration(new OrderConf());
			model_builder.ApplyConfiguration(new OrderItemConf());
		}
	}
}
