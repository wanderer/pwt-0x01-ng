using System.Collections.Generic;

namespace pwt_0x01_ng.Models.Database
{
	public static class ProductHelper
	{
		public static IList<Product> GenerateProduct()
		{
			IList<Product> products = new List<Product>()
			{
				new Product() { Name="a", Price=0, Description="aaaa", ImageSrc = "/images/banner1.svg", ImageAlt = "ASP.NET", Similar = {}},
				new Product() { Name="b", Price=1, Description="bbbb", ImageSrc = "/images/banner2.svg", ImageAlt = "ASP.NET", Similar = {}},
				new Product() { Name="c", Price=2, Description="cccc", ImageSrc = "/images/banner3.svg", ImageAlt = "ASP.NET", Similar = {}},
				new Product() { Name="d", Price=3, Description="dddd", ImageSrc = "/images/ms_loves_linux.jpeg", ImageAlt = "msloveslinux", Similar = {}}
					};
			return products;
		}
	}
}
