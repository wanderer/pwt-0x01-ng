using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using pwt_0x01_ng.Models.Identity;

namespace pwt_0x01_ng.Models
{
	[Table(nameof(Order))]
	public class Order : Entity
	{
		[Required]
		public string Order_Number { get; set; }
		public IList<OrderItem> OrderItems { get; set; }
		[ForeignKey(nameof(Identity.User))]
		[Required]
		public int User_id { get; set; }
		[NotMapped]
		public User usr { get; set; }
		[Required]
		public decimal Price_total { get; set; }
	}
}