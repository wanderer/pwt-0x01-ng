using System.Collections.Generic;

namespace pwt_0x01_ng.Models
{
	public class CarouselViewModel
	{
		public IList<Carousel> Carousels { get; set; }
	}
}
