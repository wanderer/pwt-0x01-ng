using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using pwt_0x01_ng.Models.Identity;

namespace pwt_0x01_ng.Models.ApplicationServices
{
	public interface ISecurityApplicationService
	{
		Task<string[]> register(RegisterViewModel rvm, Roles role);
		Task<bool> login(LoginViewModel lvm);
		Task logout();
		Task<User> find_user_by_username(string username);
		Task<IList<string>> gimme_user_roles(User usr);
		Task<User> gimme_current_user(ClaimsPrincipal claims);
	}
}