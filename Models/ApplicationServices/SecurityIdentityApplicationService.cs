using System.Linq;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using pwt_0x01_ng.Models.Identity;

namespace pwt_0x01_ng.Models.ApplicationServices
{
	public class SecurityIdentityApplicationService : ISecurityApplicationService
	{
		UserManager<User> um;
		SignInManager<User> sim;

		public SecurityIdentityApplicationService(UserManager<User> um, SignInManager<User> sim){
			this.um = um;
			this.sim = sim;
		}

		public async Task<string[]> register(RegisterViewModel rvm, Roles role){
			User usr = new User(){username = rvm.username, UserName = rvm.username, Name = rvm.username, LastName = "", password = rvm.password, Email = rvm.email, EmailConfirmed = true};

			string[] errors = null;
			var res = await um.CreateAsync(usr, rvm.password);

			if(res.Succeeded){
				var roleres = await um.AddToRoleAsync(usr, role.ToString());
				if(!roleres.Succeeded){
					int c = 0;
					foreach (var err in res.Errors)
					{
						res.Errors.Append(res.Errors.ElementAt(c));
						c++;
					}
				}
			}

			if(res.Errors != null && res.Errors.Count() > 0){
				errors = new string[res.Errors.Count()];
				int c = 0;
				foreach (var err in res.Errors)
				{
					errors[c] = res.Errors.ElementAt(c).Description;
				}
				c++;
			}
			return errors;
		}
		public async Task<bool> login(LoginViewModel lvm){
			var result = await sim.PasswordSignInAsync(lvm.username, lvm.password, config.remember_me_feature, config.lockout_on_failure);
			return result.Succeeded;
		}
		public Task logout(){
			return sim.SignOutAsync();
		}
		public Task<User> find_user_by_username(string username){
			return um.FindByNameAsync(username);
		}
		public Task<IList<string>> gimme_user_roles(User usr){
			return um.GetRolesAsync(usr);
		}
		public Task<User> gimme_current_user(ClaimsPrincipal claims){
			return um.GetUserAsync(claims);
		}
	}
}