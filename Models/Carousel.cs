using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using pwt_0x01_ng.Models.Validation;

namespace pwt_0x01_ng.Models
{
	[Table("Carousel")]
	public class Carousel : Entity
	{
		[Required]
		public string DataTarget { get; set; }
		[NotMapped]
		[FileTypeAttr("image")]
		public IFormFile Image { get; set; }
		[StringLength(255)]
		public string ImageSrc { get; set; }
		[Required]
		[StringLength(50, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 1)]
		public string ImageAlt { get; set; }
		[Required]
		public string CarouselContent { get; set; }
	}
}
