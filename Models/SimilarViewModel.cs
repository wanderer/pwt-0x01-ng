using System.Collections.Generic;

namespace pwt_0x01_ng.Models
{
	public class SimilarViewModel
	{
		public IList<Similar> Similar { get; set; }
	}
}