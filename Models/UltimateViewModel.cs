using System.Collections.Generic;

namespace pwt_0x01_ng.Models
{
	public class UltimateViewModel
	{
		public IList<Carousel> Carousels { get; set; }
		public IList<Product> Products { get; set; }
	}
}
