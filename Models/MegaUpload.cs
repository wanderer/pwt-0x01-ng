using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace pwt_0x01_ng.Models
{
	public class MegaUpload
	{
		string webroot_path;
		string folder_name;
		string file_type;

		public MegaUpload(string webroot_path, string folder_name, string file_type){
			this.webroot_path = webroot_path;
			this.folder_name = folder_name;
			this.file_type = file_type;
		}
		public async Task<string> DoMegaUpload(IFormFile iformfile)
		{
			string return_file_path = String.Empty;
			var img = iformfile;
			if(img != null && img.ContentType.ToLower().Contains(file_type) && img.Length > 0 && img.Length < 2000000){
				var fname = Path.GetFileNameWithoutExtension(img.FileName);
				var fext = Path.GetExtension(img.FileName);
				/* in case such file already exists - wip */
				/* var fname_rand = Path.GetRandomFileName()+Path.GetFileNameWithoutExtension(img.FileName); */
				var f_relative = Path.Combine("images",folder_name, fname + fext);
				var file_path = Path.Combine(webroot_path, f_relative);

				using (var stream = new FileStream(file_path, FileMode.Create)){
					await img.CopyToAsync(stream);
				}
				return_file_path = $"/{f_relative}";
				/* great_success = true; */
			}
			return return_file_path;
		}
	}
}