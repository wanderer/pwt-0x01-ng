using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace pwt_0x01_ng.Models
{
	public abstract class Entity
	{
		[Key]
		[Required]
		public int id { get; set; }
		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime Created { get; }
		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime Updated { get; }
	}
}