function Buy(Product_id, urlAction, outElementId, locale) {
	$.ajax({
		type: "POST",
		url: urlAction,
		data: { Product_id: Product_id },
		dataType: "text",
		success: function (price_total) {
			ChangeTotalPriceInformation(outElementId, locale, price_total);
		},
		error: function (req, status, error) {
			$(outElementId).text('error during buying expensive stuff');
		}
	});
}

function ChangeTotalPriceInformation(outElementId, locale, price_total) {
	$(outElementId).text(parseFloat(price_total).toLocaleString(locale,
		{
			style: "currency",
			currency: "EUR",
			minimumFractionDigits: 2,
			maximumFractionDigits: 2
		}));
}

function item_deletion_confirmation() {
	return confirm("Delete the item?");
}