$.validator.addMethod('content', function (value, element, params) {
	var f_type = params[1]
	uploaded_type = "";

	if (!value) {
		return true;
	}

	if (element && element.files && element.files.length > 0) {
		uploaded_type = element.files[0].type;
	}

	if (f_type && uploaded_type != "" && uploaded_type.toLowerCase().includes(f_type)) {
		return true;
	}

	return false;
});

$.validator.unobtrusive.adapters.add('content', ['type'], function (options) {
	var element = $(options.form).find('#file')[0];

	options.rules['content'] = [element, options.params['type']];
	options.messages['content'] = options.message;
});