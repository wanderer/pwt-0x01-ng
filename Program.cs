﻿using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using pwt_0x01_ng.Models.Database;

namespace pwt_0x01_ng
{
	public class Program
	{
		public static async Task Main(string[] args)
		{
			IWebHost webHost = CreateWebHostBuilder(args).Build();

			using (var scope = webHost.Services.CreateScope())
			{
				var serviceProvider = scope.ServiceProvider;
				var dbctx = serviceProvider.GetRequiredService<DBContext>();
				DBInit.Init(dbctx);
				await DBInit.EnsureRolesCreated(serviceProvider);
				await DBInit.EnsureAdminCreated(serviceProvider);
				await DBInit.EnsureManagerCreated(serviceProvider);
			}

			webHost.Run();
		}

		public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
				.UseStartup<Startup>()
				.ConfigureLogging((ctx, logging) => {
					logging.ClearProviders();
					logging.AddConsole();
					logging.AddDebug();
					logging.AddEventSourceLogger();
				});
	}
}
