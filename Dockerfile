FROM mcr.microsoft.com/dotnet/core/sdk:3.1-alpine AS base
WORKDIR /src

COPY *.csproj ./
RUN dotnet restore

COPY . ./
RUN dotnet publish -c Release -o bin/out

FROM mcr.microsoft.com/dotnet/aspnet:3.1-alpine
WORKDIR /App
COPY --from=base /src/bin/out/ .
RUN chown -R nobody:nobody ./
USER nobody
ENV ASPNETCORE_ENVIRONMENT Production
ENV ASPNETCORE_URLS http://*:8081
ENTRYPOINT ["dotnet", "pwt-0x01-ng.dll"]
