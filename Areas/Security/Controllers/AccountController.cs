using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using pwt_0x01_ng.Models.ApplicationServices;
using pwt_0x01_ng.Controllers;
using pwt_0x01_ng.Models;

namespace pwt_0x01_ng.Areas.Security.Controllers
{
	[Area("Security")]
	[AllowAnonymous]
	public class AccountController : Controller
	{
		private ISecurityApplicationService isas;

		public AccountController(ISecurityApplicationService isas){
			this.isas = isas;
		}

		public IActionResult Login()
		{
			return View();
		}

		[HttpPost]
		public async Task<IActionResult> Login(LoginViewModel lvm)
		{
			lvm.login_failed = false;
			if(ModelState.IsValid){
				bool login_success = await isas.login(lvm);
				if(login_success){
					return RedirectToAction(nameof(HomeController.Index), nameof(HomeController).Replace("Controller", String.Empty), new {area = ""});
				} else {
					lvm.login_failed = true;
				}
			}
			return View(lvm);
		}

		public IActionResult Logout()
		{
			isas.logout();
			return RedirectToRoute(new { action = nameof(HomeController.Index), controller = nameof(HomeController).Replace("Controller", String.Empty), area="" });
		}

		public IActionResult Register()
		{
			return View();
		}

		[HttpPost]
		public async Task<IActionResult> Register(RegisterViewModel rvm)
		{
			string[] errors = null;
			if(ModelState.IsValid){
				errors = await isas.register(rvm, Models.Identity.Roles.Customer);
				if(errors == null){
					var lvm = new LoginViewModel(){username = rvm.username, password = rvm.password, login_failed = false};
					return await Login(lvm);
				}
				Console.WriteLine("error registering user {" + rvm.username + "," + rvm.email + "," + rvm.password + "}.");
			}
			string err = "";
			for (int i = 0; i < ModelState.Values.Count(); i++){
				for (int j = 0; j < ModelState.Values.ElementAt(i).Errors.Count(); j++){
					err += "\n " + ModelState.Values.ElementAt(i).Errors.ElementAt(j).ToString();
				}
			}
			Console.WriteLine("ModelState errors: " + err);
			ViewData["Message"] = "error registering an account => \n" + err;
			return View();
		}
	}
}