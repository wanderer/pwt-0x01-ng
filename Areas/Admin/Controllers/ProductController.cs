using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using pwt_0x01_ng.Models;
using pwt_0x01_ng.Models.Database;
using pwt_0x01_ng.Models.Identity;
using System.Collections.Generic;

namespace pwt_0x01_ng.Areas.Admin.Controllers
{
	[Area("Admin")]
	[Authorize(Roles = nameof(Roles.Admin) + "," + nameof(Roles.Manager))]
	public class ProductController : Controller
	{
		IWebHostEnvironment hosting_env;
		readonly DBContext dbctx;

		public ProductController(DBContext dbctx, IWebHostEnvironment hosting_env){
			this.dbctx = dbctx;
			this.hosting_env = hosting_env;
		}

		public async Task <IActionResult> Select()
		{
			ProductViewModel product = new ProductViewModel();
			product.Products = await dbctx.Product.ToListAsync();
			return View(product);
		}

		public async Task<IActionResult> Create()
		{
			ProductViewModel product = new ProductViewModel();
			product.Products = await dbctx.Product.ToListAsync();
			ViewData["prods"] = product.Products;
			return View();
		}

		[HttpPost]
		public async Task<IActionResult> Create(Product product)
		{
			if (ModelState.IsValid) {
				MegaUpload mega_upload = new MegaUpload(hosting_env.WebRootPath, "products", "image");
				if ((product.ImageSrc = await mega_upload.DoMegaUpload(product.Image)) != string.Empty){/* all good here */};

				dbctx.Product.Add(product);
				await dbctx.SaveChangesAsync();
				return RedirectToAction(nameof(Select));
			} else {
				ViewData["Message"] = "error creating Product";
				return View(product);
			}
		}

		public async Task<IActionResult> Edit(int id)
		{
			ProductViewModel product = new ProductViewModel();
			product.Products = await dbctx.Product.ToListAsync();
			ViewData["prods"] = product.Products;

			Product item = dbctx.Product.Where(p_item => p_item.id == id).FirstOrDefault();
			if (item != null)
			{
				IList<Similar> s = await dbctx.Similar.Where(p_item => p_item.prod_id == id).ToListAsync();
				IList<SimilarProduct> similar = new List<SimilarProduct>();
				foreach (var prod in product.Products) {
					SimilarProduct sp = new SimilarProduct();
					sp.id = prod.id;
					sp.Selected = prod.Selected;
					similar.Add(sp);
				}
				foreach (var s_item in s)
				{
					var prodpls = await dbctx.Product.Where(p_item => p_item.id == s_item.similar_prod_id).FirstOrDefaultAsync();
					if(prodpls != null){
						for (int i = 0; i < similar.Count; i++)
						{
							if(similar[i].id == prodpls.id){
								similar[i].Selected = true;
							}
						}
					}
				}
				item.Similar = similar;
				ViewData["ImageSrc"] = new SelectList(dbctx.Product, "ImageSrc", item.ImageSrc);
				return View(item);
			}
			else
			{
				return NotFound();
			}
		}

		[HttpPost]
		public async Task<IActionResult> Edit(Product product)
		{
			Product item = dbctx.Product.Where(p_item => p_item.id == product.id).FirstOrDefault();

			if (item != null)
			{
				if (ModelState.IsValid) {
					item.id = product.id;
					item.Name = product.Name;
					item.Price = product.Price;
					item.Description = product.Description;
					item.ImageAlt = product.ImageAlt;

					MegaUpload mega_upload = new MegaUpload(hosting_env.WebRootPath, "products", "image");
					if ((product.ImageSrc = await mega_upload.DoMegaUpload(product.Image)) != string.Empty){
						item.ImageSrc = product.ImageSrc;
					}

					if (product.Similar != null) {
						IList<SimilarProduct> similar = new List<SimilarProduct>();
						foreach (var prod in product.Similar) {
							SimilarProduct sp = new SimilarProduct();
							sp.id = prod.id;
							sp.Selected = prod.Selected;
							similar.Add(sp);
						}
						if(similar.Count > 0){
							foreach (var prod in similar)
							{
								Similar s = dbctx.Similar.Where(p_item => p_item.prod_id == item.id && p_item.similar_prod_id == prod.id).FirstOrDefault();
								if(prod.Selected){
									if (s == null) {
										s = new Similar();
										s.prod_id = item.id;
										s.similar_prod_id = prod.id;
										dbctx.Similar.Add(s);
									}
								} else {
									if (s != null) {
										dbctx.Similar.Remove(s);
									}
								}
							}
						}
					}

					await dbctx.SaveChangesAsync();
					return RedirectToAction(nameof(Select));
				}
				var errors = ModelState.Values.SelectMany(v => v.Errors);
				ViewData["Message"] = "error editing Product";
				return View(item);
			}
			else
			{
				return NotFound();
			}
		}

		public async Task <IActionResult> Delete(int id)
		{
			Product item = dbctx.Product.Where(p_item => p_item.id == id).FirstOrDefault();
			if (item != null)
			{
				dbctx.Product.Remove(item);
				await dbctx.SaveChangesAsync();
				return RedirectToAction(nameof(Select));
			}
			else
			{
				return NotFound();
			}
		}
	}
}
