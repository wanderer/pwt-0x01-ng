using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using pwt_0x01_ng.Models;
using pwt_0x01_ng.Models.Database;
using pwt_0x01_ng.Models.Identity;

namespace pwt_0x01_ng.Areas.Admin.Controllers
{
	[Area("Admin")]
	[Authorize(Roles = nameof(Roles.Admin) + "," + nameof(Roles.Manager))]
	public class CarouselController : Controller
	{
		IWebHostEnvironment hosting_env;
		readonly DBContext dbctx;

		public CarouselController(DBContext dbctx, IWebHostEnvironment hosting_env){
			this.dbctx = dbctx;
			this.hosting_env = hosting_env;
		}

		// GET
		public async Task <IActionResult> Select()
		{
			CarouselViewModel carousel = new CarouselViewModel();
			carousel.Carousels = await dbctx.Carousel.ToListAsync();
			return View(carousel);
		}

		public IActionResult Create()
		{
			return View();
		}

		[HttpPost]
		public async Task<IActionResult> Create(Carousel carousel)
		{
			if (ModelState.IsValid) {
				MegaUpload mega_upload = new MegaUpload(hosting_env.WebRootPath, "carousels", "image");
				if ((carousel.ImageSrc = await mega_upload.DoMegaUpload(carousel.Image)) != string.Empty){/* all good here */}

				dbctx.Carousel.Add(carousel);
				await dbctx.SaveChangesAsync();
				return RedirectToAction(nameof(Select));
			} else {
				string errors = "";
				for (int i = 0; i < ModelState.Values.Count(); i++){
					for (int j = 0; j < ModelState.Values.ElementAt(i).Errors.Count(); j++){
						errors += "\n " + ModelState.Values.ElementAt(i).Errors.ElementAt(j).ToString();
					}
				}
				ViewData["Message"] = $"error creating Carousel => \n{errors}" + errors;
				return View(carousel);
			}
		}

		public IActionResult Edit(int id)
		{
			Carousel carousel_item = dbctx.Carousel.Where(c_item => c_item.id == id).FirstOrDefault();
			if (carousel_item != null)
			{
				ViewData["ImageSrc"] = new SelectList(dbctx.Carousel, "ImageSrc", carousel_item.ImageSrc);
				return View(carousel_item);
			}
			else
			{
				return NotFound();
			}
		}

		[HttpPost]
		public async Task<IActionResult> Edit(Carousel carousel)
		{
			Carousel carousel_item = dbctx.Carousel.Where(c_item => c_item.id == carousel.id).FirstOrDefault();

			if (carousel_item != null)
			{
				if (ModelState.IsValid) {
					carousel_item.id = carousel.id;
					carousel_item.DataTarget = carousel.DataTarget;
					carousel_item.ImageAlt = carousel.ImageAlt;
					carousel_item.CarouselContent = carousel.CarouselContent;

					MegaUpload mega_upload = new MegaUpload(hosting_env.WebRootPath, "carousels", "image");
					if ((carousel.ImageSrc = await mega_upload.DoMegaUpload(carousel.Image)) != string.Empty){
						carousel_item.ImageSrc = carousel.ImageSrc;
					}
					await dbctx.SaveChangesAsync();
					return RedirectToAction(nameof(Select));
				}
				string errors = "";
				for (int i = 0; i < ModelState.Values.Count(); i++){
					for (int j = 0; j < ModelState.Values.ElementAt(i).Errors.Count(); j++){
						errors += "\n " + ModelState.Values.ElementAt(i).Errors.ElementAt(j).ToString();
					}
				}
				ViewData["Message"] = "error editing Carousel => \n" + errors;
				return View(carousel_item);
			}
			else
			{
				return NotFound();
			}
		}

		public async Task <IActionResult> Delete(int id)
		{
			Carousel carousel_item = dbctx.Carousel.Where(c_item => c_item.id == id).FirstOrDefault();
			if (carousel_item != null)
			{
				dbctx.Carousel.Remove(carousel_item);
				await dbctx.SaveChangesAsync();
				return RedirectToAction(nameof(Select));
			}
			else
			{
				return NotFound();
			}
		}
	}
}
