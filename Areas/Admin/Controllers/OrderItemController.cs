using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using pwt_0x01_ng.Models;
using pwt_0x01_ng.Models.Database;
using pwt_0x01_ng.Models.Identity;

namespace pwt_0x01_ng.Areas.Admin.Controllers
{
	[Area("Admin")]
	[Authorize(Roles = nameof(Roles.Admin))]
	public class OrderItemController : Controller
	{
		private readonly DBContext _context;

		public OrderItemController(DBContext context)
		{
			_context = context;
		}

		// GET: Admin/OrderItem
		public async Task<IActionResult> Index()
		{
			var dBContext = _context.OrderItem.Include(o => o.Order).Include(o => o.Product);
			return View(await dBContext.ToListAsync());
		}

		// GET: Admin/OrderItem/Details/5
		public async Task<IActionResult> Details(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var orderItem = await _context.OrderItem
				.Include(o => o.Order)
				.Include(o => o.Product)
				.FirstOrDefaultAsync(m => m.id == id);
			if (orderItem == null)
			{
				return NotFound();
			}

			return View(orderItem);
		}

		// GET: Admin/OrderItem/Create
		public IActionResult Create()
		{
			ViewData["Order_id"] = new SelectList(_context.Order, "id", "Order_Number");
			ViewData["Product_id"] = new SelectList(_context.Product, "id", "Description");
			return View();
		}

		// POST: Admin/OrderItem/Create
		// To protect from overposting attacks, enable the specific properties you want to bind to, for 
		// more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create([Bind("Order_id,Product_id,Amount,Price,id,Created,Updated")] OrderItem orderItem)
		{
			if (ModelState.IsValid)
			{
				_context.Add(orderItem);
				await _context.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			ViewData["Order_id"] = new SelectList(_context.Order, "id", "Order_Number", orderItem.Order_id);
			ViewData["Product_id"] = new SelectList(_context.Product, "id", "Description", orderItem.Product_id);
			return View(orderItem);
		}

		// GET: Admin/OrderItem/Edit/5
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var orderItem = await _context.OrderItem.FindAsync(id);
			if (orderItem == null)
			{
				return NotFound();
			}
			ViewData["Order_id"] = new SelectList(_context.Order, "id", "Order_Number", orderItem.Order_id);
			ViewData["Product_id"] = new SelectList(_context.Product, "id", "Description", orderItem.Product_id);
			return View(orderItem);
		}

		// POST: Admin/OrderItem/Edit/5
		// To protect from overposting attacks, enable the specific properties you want to bind to, for 
		// more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(int id, [Bind("Order_id,Product_id,Amount,Price,id,new.Created,new.Updated")] OrderItem orderItem)
		{
			if (id != orderItem.id)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					_context.Update(orderItem);
					await _context.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!OrderItemExists(orderItem.id))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}
				return RedirectToAction(nameof(Index));
			}
			ViewData["Order_id"] = new SelectList(_context.Order, "id", "Order_Number", orderItem.Order_id);
			ViewData["Product_id"] = new SelectList(_context.Product, "id", "Description", orderItem.Product_id);
			return View(orderItem);
		}

		// GET: Admin/OrderItem/Delete/5
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var orderItem = await _context.OrderItem
				.Include(o => o.Order)
				.Include(o => o.Product)
				.FirstOrDefaultAsync(m => m.id == id);
			if (orderItem == null)
			{
				return NotFound();
			}

			return View(orderItem);
		}

		// POST: Admin/OrderItem/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			var orderItem = await _context.OrderItem.FindAsync(id);
			_context.OrderItem.Remove(orderItem);
			await _context.SaveChangesAsync();
			return RedirectToAction(nameof(Index));
		}

		private bool OrderItemExists(int id)
		{
			return _context.OrderItem.Any(e => e.id == id);
		}
	}
}
