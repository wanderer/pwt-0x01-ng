using pwt_0x01_ng.Models;
using pwt_0x01_ng.Models.ApplicationServices;
using pwt_0x01_ng.Models.Database;
using pwt_0x01_ng.Models.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pwt_0x01_ng.Areas.Customer.Controllers
{
	[Area("Customer")]
	[Authorize(Roles = nameof(Roles.Customer))]
	public class CustomerOrdersController : Controller
	{

		ISecurityApplicationService isas;
		DBContext ctx;

		public CustomerOrdersController(ISecurityApplicationService isas, DBContext ctx)
		{
			this.isas = isas;
			this.ctx = ctx;
		}

		public async Task<IActionResult> Index()
		{
			if (User.Identity.IsAuthenticated)
			{
				User currentUser = await isas.gimme_current_user(User);
				if (currentUser != null)
				{
					IList<Order> userOrders = await this.ctx.Order
						.Where(or => or.User_id == currentUser.Id)
						.Include(o => o.OrderItems)
						.ThenInclude(oi => oi.Product)
						.ToListAsync();
					string uname = currentUser.username;
					ViewData["uname"] = uname;
					return View(userOrders);
				}
			}

			return NotFound();
		}
	}
}
