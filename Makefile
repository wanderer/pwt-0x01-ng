dc = docker-compose
dccmd = COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose
dtag = netcoreultimateapp-prod
dtagdev = netcoreultimapp-dev
dfile = Dockerfile
dfiledev = $(dfile).dev
lport = 8000
lportdev = 8001
CC = dotnet
dcmd = DOCKER_BUILDKIT=1 docker
pruneargs = system prune -af
dcmdrun = $(dcmd) run --rm
wdir = /src
kanikoimg = gcr.io/kaniko-project/executor@sha256:6ecc43ae139ad8cfa11604b592aaedddcabff8cef469eda303f1fb5afe5e3034
dargskaniko = -w=$(wdir) -v $$(pwd):$(wdir):z $(kanikoimg)
kanikoargs = -c=$(wdir) --use-new-run --snapshotMode=redo --no-push
krelease = $(dcmdrun) $(dargskaniko) -f=$(dfile) $(kanikoargs)
kdebug = $(dcmdrun) $(dargskaniko) -f=$(dfiledev) $(kanikoargs)
postkanikochown = sudo chown -R $$USER:$$USER ./
pgdbcapdrop = --cap-drop NET_ADMIN --cap-drop SYS_ADMIN
pgdbenv = -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=679968312e029a806c1905c40ec331aa199a1eb86bd0b9eb04057933e449bdc9ef8ef292a39b68cafa5689c901a17266 -e POSTGRES_INITDB_ARGS="--data-checksums"
pgdbname = pgdb
pgdbports = -p 127.0.0.1:5433:5432
pgdbvol = -v pgdbdata:/var/lib/postgresql/data
pgdbargs = run -d $(pgdbcapdrop) $(pgdbenv) --name $(pgdbname) $(pgdbports) $(pgdbvol) --restart unless-stopped
pgdbimg = postgres:13.1-alpine
zenv = DB_CONNECTION_STRING=$$(cat appsettings.Development.json | jq .db.Postgres | sed -e 's/5432/5433/' -e 's/=db/=localhost/' -e 's/"//g')

.PHONY: dev dockerbuild dockerdevbuild dockerrun dockerdevrun dockertest dockerdev kaniko clean prune pgdba pgdbz test dcdevb dcprodbuild dcdevup dcprodup

dev: restore build run

restore:
	$(CC) restore

build:
	$(CC) build .

run:
	$(zenv) $(CC) watch run .

releasebuild: restore clean
	$(CC) publish -c Release

dockerbuild:
	$(dcmd) build \
		--build-arg VCS_REF=`git rev-parse --short HEAD` \
		--build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` \
		-t $(dtag) -f $(dfile) --no-cache .

dockerdevbuild:
	$(dcmd) build \
		--build-arg VCS_REF=`git rev-parse --short HEAD` \
		--build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` \
		-t $(dtagdev) -f $(dfiledev) --no-cache .

dockerrun:
	@echo ======================
	@echo local port: $(lport)
	@echo ======================
	$(dcmdrun) -p $(lport):80 $(dtag)

dockerdevrun:
	@echo ======================
	@echo local dev port: $(lportdev)
	@echo ======================
	$(dcmdrun) -p $(lportdev):5000 $(dtagdev)

dcdevb:
	$(dccmd) -f $(dc).yml build --no-cache --pull --progress tty

dcprodbuild:
	$(dccmd) -f $(dc).prod.yml build --no-cache --pull --progress tty

dcdevup:
	@echo ======================
	@echo local dev port: $(lportdev)
	@echo ======================
	$(dccmd) -f $(dc).yml up --remove-orphans

dcprodup:
	$(dccmd) -f $(dc).prod.yml up --remove-orphans --scale netcoreultimateapp-prod=2

kaniko:
	$(krelease)
	$(postkanikochown)
	$(kdebug)
	$(postkanikochown)

dockerdev: dockerdevbuild dockerdevrun

dockertest: dockerdevbuild dockerbuild

test: releasebuild build dockertest kaniko

clean:
	$(CC) clean

prune:
	$(dcmd) $(pruneargs)

pgdba:
	$(dcmd) $(pgdbargs) $(pgdbimg)

pgdbz:
	$(dcmd) stop $(pgdbname)
