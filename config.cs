namespace pwt_0x01_ng
{
	public static class config
	{
		public static int min_passwd_length = 2;
		public static int min_passwd_unique_chars = 1;
		public static bool lockout_on_failure = true;
		public static bool remember_me_feature = false;
	}
}